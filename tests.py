from subprocess import Popen, PIPE

inputs = ["first word", "second word", "third word", "ahaha", "L" * 256, "L" * 257]
outputs = ["first word explanation", "second word explanation", "third word explanation", "", "L word explanation", ""]
errors = ["", "", "", "That label doesn`t exists", "", "The buffer is overflowed. Max - 255 symbols"]

script_path = "./main"

print("TESTING")
print(f"script: {script_path} ")
print("summary: ", end="")
for i in range(len(inputs)):
    process = Popen([script_path], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    data = process.communicate(inputs[i].encode())
    if data[0].decode().strip() == outputs[i] and data[1].decode().strip() == errors[i]:
        print("TEST {i} PASSED\n")
    else:
        print("TEST {i} FAILED : \n" + data[1].decode().strip())
