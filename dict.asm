%include "lib.inc"

%define NEXT_ELEM 8

%define SOURCE_PTR r12
%define WORD_PTR r13

global find_word

section .text

find_word:
    push SOURCE_PTR
    push WORD_PTR

    mov SOURCE_PTR, rdi
    mov WORD_PTR, rsi

    .search_loop:
        mov rdi, SOURCE_PTR
        mov rsi, WORD_PTR
        add rsi, NEXT_ELEM
        call string_equals

        test rax, rax 
        jnz .found_success 

        mov WORD_PTR, [WORD_PTR] 
        test WORD_PTR, WORD_PTR 
        jnz  .search_loop 

    .not_found:
        xor rax, rax
        jmp .exit

    .found_success:
        mov rax, WORD_PTR

    .exit:
        pop WORD_PTR
        pop SOURCE_PTR
        ret
